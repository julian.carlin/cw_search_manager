# cw_search_manager

A CW search manager written in python. Mostly with the LMXB Viterbi searches in mind.

# Running

First you should load git and anaconda, source patrick clearwater's lalapps environment and then source this python virtualenv.

```bash
module load git/2.18.0
module load anaconda3/5.1.0
. /fred/oz022/lalapps/2018-03/installation/etc/lalapps-user-env.sh
source activate /fred/oz022/xray_gw_work/conda_env/gwxray3
```


