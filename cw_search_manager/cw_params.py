import configparser
import os
import subprocess as sp
import logging

# from .utils import gw_waveform
import glob
import numpy as np

# from lalpulsar import simulateCW
# from gwpy.timeseries import TimeSeries
# from gwpy.segments import DataQualityDict
import numpy as np
from pathlib import Path

"""
Set of classes for managing CW searches.
"""


class ParamsBase(dict):
    """docstring for ParamsBase"""

    def __init__(self, *args, **kwargs):
        super(ParamsBase, self).__init__(*args, **kwargs)
        self.__dict__ = self


class Params(ParamsBase):
    """param object"""

    # allowed sections of params
    allowed_sections = [
        "search",
        "injection",
        "post process",
        "viterbi",
        "H1-sft",
        "L1-sft",
        "V1-sft",
    ]

    def __init__(self, *args, **kwargs):
        super(Params, self).__init__(*args, **kwargs)

    @classmethod
    def from_config_file(cls, cfile):
        if not os.path.isfile(cfile):
            raise ValueError("config file {0} not found".format(cfile))
        config = configparser.RawConfigParser()
        config.optionxform = str
        config.read(cfile)
        params = cls()
        if "injection" in config:
            params.injection = InjectionParams(**config["injection"])
            print("Running on simulated data.")
        if "search" in config:
            params.search = SearchParams(**config["search"])
        if "viterbi" in config:
            params.viterbi = ViterbiParams(**config["viterbi"])
            print("Running full Viterbi search.")
        if "H1-sft" in config:
            params.h1sft = SFTParams(**config["H1-sft"])
        if "L1-sft" in config:
            params.l1sft = SFTParams(**config["L1-sft"])
        if "V1-sft" in config:
            params.v1sft = SFTParams(**config["V1-sft"])
        return params

    def __str__(self):
        """
        print string for params class.
        """
        msg = "Parameters currently defined\n"
        msg += "-" * len(msg)
        msg += "\n\n\t"
        if "injection" in self:
            msg += self.injection.__str__().replace("\n", "\n\t")
        if "search" in self:
            msg += self.search.__str__().replace("\n", "\n\t")
        if "viterbi" in self:
            msg += self.viterbi.__str__().replace("\n", "\n\t")
        if "post processing" in self:
            msg += self.post_processing.__str__().replace("\n", "\n\t")
        return msg


class WeaveParams(ParamsBase):
    valid_params_weave = {
        "setup-file": (str, None),
        "output-file": (str, None),
        "alpha": (float, None),
        "delta": (float, None),
        "freq": (str, None),
        "f1dot": (str, None),
        "f2dot": (str, None),
        "semi-max-mismatch": (float, 0.02),
        "sft-files": (str, None),
        "extra-statistics": (str, "coh2F"),
        "toplist-limit": (int, 1000),
        "detectors": (str, None),
        "spindowns": (int, None),
        "segment-list": (str, None),
        "ref-time": (str, None),
        "injection": (str, None),
    }

    def __init__(self, *args, **kwargs):
        super(WeaveParams, self).__init__(*args, **kwargs)
        # fill in defaults
        for key, tup in WeaveParams.valid_params_weave.items():
            if key in self:
                continue
            else:
                self[key] = tup[1]

        # check params and make data types correct
        self.check_search_params()

    def check_search_params(self):
        for key, val in self.items():
            # pass over search type key
            # check wewave params
            if key in WeaveParams.valid_params_weave:
                if val is not None:
                    self[key] = WeaveParams.valid_params_weave[key][0](val)
            else:
                msg = "{0} is not a valid parameter for search type {1}"
                raise ValueError(msg.format(key, "weave"))

    def __str__(self):
        msg = "Weave Search Parameters\n"
        msg += "-" * len(msg) + "\n"
        for key, val in self.items():
            msg += "{0:<15}: {1}\n".format(key, val)
        return msg

    def run_search(self, printcmd=True, check=True):
        opt = "--{0} {1} "
        args = ""
        # setup
        setup_cmd = f"lalapps_WeaveSetup -L {self['segment-list']} -r {self['ref-time']} --output-file {self['setup-file']} --detectors {self['detectors']} --spindowns {self['spindowns']}"
        setup_keys = ["segment-list", "ref-time", "detectors", "spindowns"]
        search_cmd = "lalapps_Weave"
        for key, val in self.items():
            cont = False
            for sk in setup_keys:
                if key == sk:
                    # this is a setup key, don't include it in search command
                    cont = True
                    break
            if cont:
                continue
            elif val is None:
                continue
            else:
                search_cmd = f"{search_cmd} --{key} {val}"

        if printcmd:
            print(setup_cmd)
            print(search_cmd)
        cp = sp.run(
            setup_cmd, shell=True, check=check
        )  #  capture_output=True, text=True)
        cp = sp.run(
            search_cmd, shell=True, check=check
        )  # , capture_output=True, text=True)


class SearchParams(ParamsBase):
    """docstring for SearchParams"""

    # list of valid parameters
    valid_type_params = {"fstatistic", "jstatistic", "jstatistic_gpu"}
    valid_params_fstatistic = {
        "FreqBand": (float, None),
        "Alpha": (float, None),
        "Delta": (float, None),
        "Freq": (float, None),
        "dFreq": (float, None),
        "outputFstat": (str, "results.fstat"),
        "DataFiles": (str, None),
        "f1dot": (float, None),
        "df1dot": (float, None),
        "f1dotBand": (float, None),
        "f2dotBand": (float, None),
        "f3dotBand": (float, None),
        "gridType": (int, None),
        "metricType": (int, None),
        "FstatMethod": (str, None),
        "metricMismatch": (float, None),
        "refTime": (float, None),
        "f2dot": (float, None),
        "f3dot": (float, None),
        "minStartTime": (float, None),
        "maxStartTime": (float, None),
        "SFTCache": (str, None),
        "orbitasini": (float, None),
        "orbitPeriod": (float, None),
        "orbitTp": (float, None),
        "orbitEcc": (float, None),
        "orbitArgp": (float, None),
        "ephemEarth": (str, None),
        "ephemSun": (str, None),
        "countTemplates": (str, None),
    }

    valid_params_jstatistic = {
        "alpha": (float, None),
        "delta": (float, None),
        "minStartTime": (int, None),
        "maxStartTime": (int, None),
        "freqStart": (float, None),
        "freqBand": (float, None),
        "bandWingSize": (float, None),
        "dFreq": (float, None),
        "dataFiles": (str, None),
        "outputMode": (str, None),
        "Jthreshold": (float, None),
        "ephemEarth": (str, None),
        "ephemSun": (str, None),
        "asini": (float, None),
        "asini-step": (float, None),
        "asini-end": (float, None),
        "orbital-P": (float, None),
        "phi": (float, None),
        "phi-step": (float, None),
        "phi-end": (float, None),
        "orbitTp": (float, None),
        "orbitTp-step": (float, None),
        "orbitTp-end": (float, None),
        "driftTime": (int, None),
        "scoresFile": (str, None),
        "rngMedWindow": (int, None),
        "viterbiOutputDirectory": (str, None),
        "viterbiThreshold": (float, None),
        "saveFstatAtoms": (str, None),
        "loadFstatAtoms": (str, None),
        "outputJstat": (str, None),
        "savedJstat": (str, None),
        "deltaFile": (str, None),
        "CJSversion": (str, None),
    }

    valid_params_jstatistic_gpu = {
        "atoms": (str, None),
        "cjs_atoms": (str, None),
        "start_time": (int, None),
        "tblock": (int, None),
        "central_a0": (float, None),
        "a0_band": (float, None),
        "a0_bins": (int, None),
        "P": (float, None),
        "central_P": (float, None),
        "P_band": (float, None),
        "P_bins": (int, None),
        "central_phase": (float, None),
        "phase_band": (float, None),
        "phase_bins": (int, None),
        "central_orbitTp": (float, None),
        "orbitTp_band": (float, None),
        "orbitTp_bins": (int, None),
        "block_size": (int, None),
        "ignore_wings": (int, None),
        "fstat": (str, None),
        "out_prefix": (str, None),
        "obs_len": (int, None),
        "use_callback": (str, None),
        "print_ll": (str, None),
        "threshold": (float, None),
        "llthreshold": (float, None),
        "gpu_executable": (str, None),
    }

    def _translate_fkdot(self, fkdot, t_new, t_old):
        fkdot_new = np.zeros(fkdot.size)
        delta_t = t_new - t_old
        # translate to new reference time
        for s in range(fkdot.size):
            for m in range(s, fkdot.size):
                fkdot_new[s] += (
                    fkdot[m] * (delta_t) ** (m - s) / np.math.factorial(m - s)
                )
        return fkdot_new

    def translate_to_new_reference_time(self, t_new):
        """
        translate search to new reference time
        """
        fkdot = []
        fkdot_band = []
        fkdot_names = ["Freq", "f1dot", "f2dot"]  # , 'f3dot']
        fkdot_band_names = ["FreqBand", "f1dotBand", "f2dotBand"]  # ,'f3dotBand']
        # set up vector of fdot variables
        for fn in fkdot_names:
            if self[fn] is not None:
                fkdot.append(self[fn])
            else:
                fkdot.append(0)
        fkdot = np.asarray(fkdot)
        t_old = self["refTime"]
        fkdot_new = self._translate_fkdot(fkdot, t_new, t_old)
        # translate to new reference time
        # reset frequency derivatives
        # reset reference time
        # Now do the same thing for the freuqency bands
        for fn in fkdot_band_names:
            if self[fn] is not None:
                fkdot_band.append(self[fn])
            else:
                fkdot_band.append(0)
        fkdot_band = np.asarray(fkdot_band)
        fkdot_higher_new = self._translate_fkdot(fkdot + fkdot_band, t_new, t_old)
        # new band
        new_band = fkdot_higher_new - fkdot_new

        for ii, fn in enumerate(fkdot_names):
            self[fn] = fkdot_new[ii]
        for ii, fn in enumerate(fkdot_band_names):
            self[fn] = new_band[ii]

        self["refTime"] = t_new

    def __init__(self, *args, **kwargs):
        super(SearchParams, self).__init__(*args, **kwargs)
        if "search_type" not in self:
            logging.warning("search type not set...setting it to F-statistic")
            self.search_type = "fstatistic"

        # fill in defaults
        if self.search_type == "jstatistic":
            for key, tup in SearchParams.valid_params_jstatistic.items():
                if key in self:
                    continue
                else:
                    self[key] = tup[1]
        elif self.search_type == "fstatistic":
            for key, tup in SearchParams.valid_params_fstatistic.items():
                if key in self:
                    continue
                else:
                    self[key] = tup[1]

        elif self.search_type == "jstatistic_gpu":
            for key, tup in SearchParams.valid_params_jstatistic_gpu.items():
                if key in self:
                    continue
                else:
                    self[key] = tup[1]

        else:
            raise ValueError(
                'search_type parameter must be "fstatistic", "jstatistic", or "jstatistic_gpu"'
            )

        # check
        self.check_search_params()

    def check_search_params(self):
        """
        self checker. make sure only supported params are defined
        """
        # loop over keys
        for key, val in self.items():
            # pass over search type key
            if key == "search_type" or key == "outputJstat":
                continue
            # check fstatistic
            if (
                self.search_type == "fstatistic"
                and key in SearchParams.valid_params_fstatistic
            ):
                if val is not None:
                    self[key] = SearchParams.valid_params_fstatistic[key][0](val)
            # check jstatistic
            elif (
                self.search_type == "jstatistic"
                and key in SearchParams.valid_params_jstatistic
            ):
                if val is not None:
                    self[key] = SearchParams.valid_params_jstatistic[key][0](val)
            elif (
                self.search_type == "jstatistic_gpu"
                and key in SearchParams.valid_params_jstatistic_gpu
            ):
                if val is not None:
                    self[key] = SearchParams.valid_params_jstatistic_gpu[key][0](val)

            else:
                msg = "{0} is not a valid parameter for search type {1}"
                raise ValueError(msg.format(key, self.search_type))

    def __str__(self):
        if self.search_type == "fstatistic":
            msg = "F-statistic Search Parameters\n"
            msg += "-" * len(msg) + "\n"
            for key, val in self.items():
                msg += "{0:<15}: {1}\n".format(key, val)
            return msg
        elif self.search_type == "jstatistic":
            msg = "J-statistic Search Parameters\n"
            msg += "-" * len(msg) + "\n"
            for key, val in self.items():
                msg += "{0:<15}: {1}\n".format(key, val)
            return msg
        elif self.search_type == "jstatistic_gpu":
            msg = "J-statistic Search Parameters\n"
            msg += "-" * len(msg) + "\n"
            for key, val in self.items():
                msg += "{0:<15}: {1}\n".format(key, val)
            return msg

        else:
            raise ValueError(
                '"search_type" must be "fstatistic", "jstatistic", or "jstatistic_gpu"'
            )

    @classmethod
    def print_defaults(cls, search_type="fstatistic"):
        """
        print default parameters
        """
        if search_type == "fstatistic":
            msg = "LAL_DEBUG_LEVEL=MSGLVL2 lalapps_ComputeFstatistic_v2 supported parameters"
            print(msg)
            print("-" * len(msg))
            for key, tup in cls.valid_params_fstatistic.items():
                print("{0:<15}: {1:<18}{2}".format(key, str(tup[0]), tup[1]))
            print("\n")
        elif search_type == "jstatistic":
            msg = "lalapps_ComputeJstatistic supported parameters"
            print(msg)
            print("-" * len(msg))
            for key, tup in cls.valid_params_jstatistic.items():
                print("{0:<15}: {1:<18}{2}".format(key, str(tup[0]), tup[1]))
            print("\n")
        elif search_type == "jstatistic_gpu":
            msg = "J-statistic GPU supported parameters"
            print(msg)
            print("-" * len(msg))
            for key, tup in cls.valid_params_jstatistic.items():
                print("{0:<15}: {1:<18}{2}".format(key, str(tup[0]), tup[1]))
            print("\n")

        else:
            raise ValueError(
                '"search_type" must be "fstatistic", "jstatistic", or "jstatistic_gpu"'
            )

    def apply_metric(self, mismatch=None):
        """
        apply metric based on Equation (70) in:
        https://arxiv.org/pdf/1502.00914.pdf

        Changes SearchParams object in place by adding step sizes
        to orbital period, etc.

        Parameters:
        -----------
        mismatch : `float`
            mismatch value

        Returns:
        --------
        """

        # TODO maybe add steps in orbital period eventually
        if mismatch is None:
            logging.info("Mismatch not supplied...setting it to 0.1")
            mismatch = 0.1

        Da0 = self["asini-end"] - self["asini"]
        Na0 = (
            np.ceil(
                ((np.pi * np.sqrt(2)) / 2)
                * ((mismatch) ** (-1 / 2))
                * (self.freqStart + self.freqBand / 2.0)
                * Da0
            )
            + 1
        )
        DT = self["orbitTp-end"] - self["orbitTp"]
        Dphi = np.mod((2 * np.pi * (DT)) / self["orbital-P"], 2 * np.pi)
        Nphi = np.ceil(
            ((np.pi * np.sqrt(2)) / 2)
            * ((mismatch) ** (-1 / 2))
            * (self.freqStart + self.freqBand / 2.0)
            * (self["asini-end"])
            * Dphi
        )
        self["orbitTp-step"] = DT / Nphi
        self["asini-step"] = Da0 / Na0

    @property
    def search_command(self):
        if self["search_type"] == "jstatistic_gpu":
            opt = "--{0}={1} "
        else:
            opt = "--{0} {1} "
        args = ""
        for key, val in self.items():
            if (
                key == "search_type"
                or key == "outputJstat"
                or key == "gpu_executable"
                or key == "CJSversion"
            ):
                continue
            if val is not None:
                args += opt.format(key, val)
        if self.search_type == "fstatistic":
            cmd = "LAL_DEBUG_LEVEL=MSGLVL2 lalapps_ComputeFstatistic_v2 {0}".format(
                args
            )
        elif self.search_type == "jstatistic":
            if self["CJSversion"] == "Julian":
                cmd = "lalapps_ComputeJStatistic.Julian {0}".format(args)
            else:
                cmd = "lalapps_ComputeJStatistic {0}".format(args)
        elif self.search_type == "jstatistic_gpu":
            cmd = "{0} {1}".format(self["gpu_executable"], args)
        else:
            raise ValueError('"search_type" must be "jstatistic", or "jstatistic_gpu"')
        # if searcy type is jstatistic and outputJstat is set
        if self.search_type == "jstatistic" and self.outputJstat is not None:
            cmd += "> " + self.outputJstat
        return cmd

    def upper_limit_command(self, loudest_2F, sft_patt, output_file):
        cmd = "echo %s | tr ' ' ';'" % sft_patt[1:-1]
        sft_patt = "'" + sp.check_output(cmd, shell=True).strip().decode("utf-8") + "'"
        opt = "--{0} {1} "
        # add arguments
        args = ""
        args += opt.format("freq", self.Freq)
        args += opt.format("freq-band", self.FreqBand)
        args += opt.format("loudest-2F", loudest_2F)
        args += opt.format("sft-patt", sft_patt)
        args += opt.format("output-file", output_file)
        args += opt.format("alpha", self.Alpha)
        args += opt.format("delta", self.Delta)
        if self.search_type == "fstatistic":
            cmd = "lalapps_ComputeFstatMCUpperLimit {0}".format(args)
        else:
            raise ValueError('"search_type" must be "fstatistic" for MCMC upper limit')
        # if searcy type is jstatistic and outputJstat is set
        return cmd

    def run_gw_search(self, printcmd=False, outputfile=None):
        if printcmd:
            print(self.search_command)
        if outputfile:
            returncode = sp.Popen(self.search_command, shell=True, stdout=outputfile)
        try:
            cp = sp.run(
                self.search_command,
                shell=True,
                check=True,
                capture_output=True,
                text=True,
            )
        except sp.CalledProcessError as cpr:
            print(" -- subprocess failed with return code", cpr.returncode)
            cp = cpr  # so the finally block can access it
            raise  # punt this upstairs
        finally:
            print(" -- subprocess standard output:")
            print(cp.stdout)
            print(" -- subprocess standard error:")
            print(cp.stderr)
            print(" -- ")
        # if returncode != 0:
        #     raise ValueError('bash script failed with return code %d' % returncode)

    def clean_gw_search(self):
        if self.search_type == "fstatistic":
            os.remove(self.outputFstat)

    def clean_atoms(self):
        if self.search_type == "jstatistic":
            files = glob.glob(self.saveFstatAtoms.replace("%d", "*"))
            for fyle in files:
                os.remove(fyle)


class InjectionParams(ParamsBase):
    """docstring for InjectionParams"""

    valid_params_v5 = {
        "outSingleSFT": (str, None),
        "outSFTdir": (str, None),
        "outLabel": (str, None),
        "TDDfile": (str, None),
        "logfile": (str, None),
        "IFOs": (str, None),
        "sqrtSX": (float, None),
        "ephemEarth": (str, None),
        "ephemSun": (str, None),
        "startTime": (float, None),
        "duration": (int, None),
        "timestampsFiles": (str, None),
        "fmin": (float, None),
        "Band": (float, None),
        "Tsft": (float, None),
        "SFToverlap": (float, None),
        "SFTWindowType": (str, None),
        "SFTWindowBeta": (float, None),
        "Alpha": (float, None),
        "Delta": (float, None),
        "Freq": (float, None),
        "refTime": (float, None),
        "h0": (float, None),
        "cosi": (float, None),
        "aPlus": (float, None),
        "aCross": (float, None),
        "psi": (float, None),
        "phi0": (float, None),
        "f1dot": (float, None),
        "f2dot": (float, None),
        "f3dot": (float, None),
        "f4dot": (float, None),
        "f5dot": (float, None),
        "f6dot": (float, None),
        "orbitTp": (float, None),
        "orbitArgp": (float, None),
        "orbitasini": (float, None),
        "orbitEcc": (float, None),
        "orbitPeriod": (float, None),
        "noiseSFTs": (str, None),
        "outFrameDir": (str, None),
        "inFrames": (str, None),
        "inFrChannels": (str, None),
        "outFrChannels": (str, None),
        "randSeed": (int, None),
        "sourceDeltaT": (float, None),
        "injectionSourcesFile": (str, None),
    }

    v5_source_parameters = {
        "Alpha": (float, None),
        "Delta": (float, None),
        "Freq": (float, None),
        "refTime": (float, None),
        "h0": (float, None),
        "cosi": (float, None),
        "aPlus": (float, None),
        "aCross": (float, None),
        "psi": (float, None),
        "phi0": (float, None),
        "f1dot": (float, None),
        "f2dot": (float, None),
        "f3dot": (float, None),
        "f4dot": (float, None),
        "f5dot": (float, None),
        "f6dot": (float, None),
        "orbitTp": (float, None),
        "orbitArgp": (float, None),
        "orbitasini": (float, None),
        "orbitEcc": (float, None),
        "orbitPeriod": (float, None),
    }

    valid_params_v4 = {
        "outSingleSFT": (str, None),
        "outSFTbname": (str, None),
        "IFO": (str, None),
        "fmin": (float, None),
        "Band": (float, None),
        "Alpha": (float, None),
        "Tsft": (float, None),
        "Delta": (float, None),
        "h0": (float, None),
        "cosi": (float, None),
        "psi": (float, 0),
        "phi0": (float, 0),
        "Freq": (float, None),
        "noiseSqrtSh": (float, None),
        "refTime": (float, None),
        "startTime": (int, None),
        "duration": (int, None),
        "orbitPeriod": (float, None),
        "orbitasini": (float, None),
        "orbitTp": (float, None),
        "orbitArgp": (float, None),
        "orbitEcc": (float, None),
        "f1dot": (float, None),
        "f2dot": (float, None),
        "f3dot": (float, None),
        "randSeed": (int, None),
        "ephemEarth": (str, None),
        "ephemSun": (str, None),
        "noiseSFTs": (str, None),
        "aPlus": (float, None),
        "aCross": (float, None),
        "window": (str, None),
        "timestampsFile": (str, None),
    }
    # TODO: add error handling so that either h0 or aPlus/aCross are specified
    # if both are specified give an error.

    def __init__(self, *args, **kwargs):
        super(InjectionParams, self).__init__(*args, **kwargs)
        # set defaults
        if "mfd_version" not in self:
            self["mfd_version"] = "v5"
        if self["mfd_version"] == "v5":
            for key, tup in InjectionParams.valid_params_v5.items():
                if key in self:
                    continue
                else:
                    self[key] = tup[1]
        elif self["mfd_version"] == "v4":
            for key, tup in InjectionParams.valid_params_v4.items():
                if key in self:
                    continue
                else:
                    self[key] = tup[1]
        self.check_injection_params()

    def __str__(self):
        msg = "Injection Parameters\n"
        msg += "-" * len(msg) + "\n"
        for key, val in self.items():
            msg += "{0:<15}: {1}\n".format(key, val)
        return msg

    @classmethod
    def print_defaults(cls):
        """
        print default parameters
        """
        msg = "lalapps_Makefakedata_v4 supported parameters"
        print(msg)
        print("-" * len(msg))
        for key, tup in cls.valid_params.items():
            print("{0:<15}: {1:<18}{2}".format(key, str(tup[0]), tup[1]))
        print("\n")

    @property
    def injection_command(self):
        opt = "--{0} {1} "
        if self["mfd_version"] == "v4":
            args = ""
            for key, val in self.items():
                if val is not None:
                    args += opt.format(key, val)
            cmd = "lalapps_Makefakedata_v4 {0}".format(args)
            return cmd
        elif self["mfd_version"] == "v5":
            # for formatting entries to injectionSources
            injsource_format = "{0}={1};"
            # for final (single) injection source str
            injsource_entry = ""
            # arguments for command
            args = ""
            injsource_done = False
            for key, val in self.items():
                if key == "mfd_version":
                    continue
                if (
                    key == "injectionSourcesFile"
                    and self["injectionSourcesFile"] is not None
                ):
                    args += opt.format("injectionSources", self[key])
                    injsource_done = True
                elif val is not None:
                    if key not in InjectionParams.v5_source_parameters:
                        args += opt.format(key, val)
                    elif key in InjectionParams.v5_source_parameters:
                        injsource_entry += injsource_format.format(key, self[key])
            if not injsource_done:
                # make injection string and add it.
                injsource_entry = "'{" + injsource_entry + "}'"
                args += opt.format("injectionSources", injsource_entry)
            cmd = "lalapps_Makefakedata_v5 {0}".format(args)
            return cmd

    def make_gw_injection(self, printcmd=False):
        # logging.info(self.injection_command)
        sp.call(self.injection_command, shell=True)

    def clean_gw_injection(self):
        if self["mfd_version"] == "v4":
            os.remove(self.outSFTbname)
        elif self["mfd_version"] == "v5":
            # get SFTs in directory
            fmt_str = "*.sft"
            files = Path(self.outSFTdir).glob(fmt_str)
            print(str(self.outSFTdir))
            # print(fmt_str)
            # remove those SFTs
            for fyle in files:
                os.remove(fyle)
            os.rmdir(self.outSFTdir)

    # def get_simulator_object(self):
    #     wf = gw_waveform(self.h0, self.cosi, self.Freq, self.f1dot)
    #     S = simulateCW.CWSimulator(self.refTime, self.startTime, self.duration,
    #             wf, 1, self.phi0, self.psi, self.Alpha, self.Delta,
    #             self.IFO)
    #     return S

    # def get_gw_strain(self, fs=None):
    #     """
    #     fs = sample rate
    #     """
    #     if fs is None:
    #         raise ValueError('Must set sample rate')
    #     S = self.get_simulator_object()
    #     ts, data = S.get_strain(fs=fs)
    #     return TimeSeries(data, t0=ts, sample_rate=fs,
    #             channel='{0}-FAKE-DATA'.format(self.IFO))

    # TODO add option for saving sfts from here.
    # TODO add option for saving frames.

    def check_injection_params(self):
        """
        self checker. make sure only supported params are defined
        """
        # loop over keys
        version = None
        try:
            version = self["mfd_version"]
        except KeyError:
            version = "v4"
        for key, val in self.items():
            # pass over search type key
            # check injection parameters
            if version == "v4":
                if key in InjectionParams.valid_params_v4:
                    if val is not None:
                        # make sure they have the correct data type
                        self[key] = InjectionParams.valid_params_v4[key][0](val)
            elif version == "v5":
                if key in InjectionParams.valid_params_v5:
                    if val is not None:
                        # make sure they have the correct data type
                        try:
                            self[key] = InjectionParams.valid_params_v5[key][0](val)
                        except ValueError:
                            raise ValueError(
                                f"{key}, {val} pair do not have correct data type specified."
                            )
            else:
                msg = "{0} is not a valid parameter"
                raise ValueError(msg.format(key))


class PostProcessingParams(ParamsBase):
    """docstring for PostProcessingParams"""

    valid_params = {}

    def __init__(self, *args, **kwargs):
        super(PostProcessingParams, self).__init__(*args, **kwargs)


class SFTParams(ParamsBase):
    """params for generated SFTs"""

    valid_sft_params = {
        "high-pass-freq": (float, 15),
        "sft-duration": (float, 1800),
        "sft-write-path": (str, "./"),
        "frame-cache": (str, "mycache"),
        "gps-start-time": (int, None),
        "gps-end-time": (int, None),
        "channel-name": (str, None),
        "sft-version": (int, 2),
        "comment-field": (str, None),
        "start-freq": (float, 20),
        "band": (float, 2000),
        "make-gps-dirs": (int, None),
        "make-tmp-file": (int, None),
        "make-desc": (str, None),
        "window-type": (int, None),
        "window-radius": (float, None),
        "overlap-fraction": (float, None),
        "dq-flags": (str, None),
    }
    valid_datafind_params = {
        "observatory": (str, None),
        "type": (str, None),
    }

    def __init__(self, *args, **kwargs):
        super(SFTParams, self).__init__(*args, **kwargs)
        for key, tup in SFTParams.valid_sft_params.items():
            if key in self:
                continue
            else:
                self[key] = tup[1]
        for key, tup in SFTParams.valid_datafind_params.items():
            if key in self:
                continue
            else:
                self[key] = tup[1]

    @property
    def cache_file_command(self):
        opt = "--{0} {1} "
        cmd = "gw_data_find "
        keys = ["observatory", "type", "gps-start-time", "gps-end-time"]
        # add params
        for key in keys:
            cmd += opt.format(key, self[key])
        cmd += "--lal-cache --url-type 'file' > " + self["frame-cache"]
        return cmd

    def make_cache_file(self):
        returncode = sp.call(self.cache_file_command, shell=True)
        if returncode != 0:
            raise ValueError("bash script failed")


#    def make_sft_plan(self, return_segments=False):
#        # get dq flags. for now just observation intent
#        # should be supplied, or things we want *on*
#        # no veto-definer handling at the moment.
#        flags = self['dq-flags'].split(',')
#        dqdict = DataQualityDict.query(flags, self['gps-start-time'],
#                                       self['gps-end-time'])
#        # get intersection of segments
#        newflag = dqdict.intersection()
#        cmds = []
#        opts = '--{0} {1} '
#        cmd_start = 'lalapps_MakeSFTs '
#        for seg in newflag.active:
#            newcmd = cmd_start
#            for key in self.keys():
#                if key == 'dq-flags':
#                    continue
#                if self[key] is None:
#                    continue
#                if key in SFTParams.valid_sft_params:
#                    if key == 'gps-start-time':
#                        newcmd += opts.format(key, seg[0])
#                    elif key == 'gps-end-time':
#                        newcmd += opts.format(key, seg[1])
#                    else:
#                        newcmd += opts.format(key, self[key])
#                if key == 'observatory':
#                    newcmd += opts.format('ifo', self[key]+'1')
#
#            cmds.append(newcmd+'-H')
#        if return_segments:
#            return cmds, newflag
#        else:
#            return cmds
#
#    def make_sfts(self):
#        # check that cache file exists
#        if not os.path.isfile(self['frame-cache']):
#            raise ValueError('frame cache must exist')
#        self.make_cache_file()
#        plan = self.make_sft_plan()
#        for cmd in plan:
#            sp.call(cmd, shell=True)
#
# Stuff for Viterbi, but not sure how to standarize this the best because there's no single version of Viterbi out there.
class ViterbiParams(ParamsBase):
    """
      Parameters for Viterbi search
    """

    valid_params = {
        "Tobs": (float, None),
        "Tdrift": (float, 864000.0),  # IN SECONDS! Default is 10 days
        "Nsteps": (int, None),
        "outputDir": (str, "viterbi_results"),
        "viterbi_executable": (str, None),
        "ldas_matlab_init": (str, None),
        "subbandIdx": (int, 0),
        "subbandWidth": (float, 1.0),
        "subbandOffset": (float, 0.0),
        "Fstat-dir": (str, None),
        "cleanFstat": (bool, False),
        "SFTCacheFiles": (str, None),
        "SFTCache": (dict, None),
    }

    def __init__(self, *args, **kwargs):
        super(ViterbiParams, self).__init__(*args, **kwargs)
        # set defaults
        for key, tup in ViterbiParams.valid_params.items():
            if key in self:
                continue
            else:
                self[key] = tup[1]

        # Initialize Nstep
        self.Nsteps = int(float(self.Tobs) / float(self.Tdrift))

        # Open and store cache files
        if self.SFTCacheFiles:
            self.SFTCache = {}
            cachelist = self.SFTCacheFiles.split(",")
            for ifo in range(len(cachelist)):
                cachenames = ["IFO", "type", "gpsStart", "duration", "path"]
                key = "IFO%i" % ifo
                try:
                    self.SFTCache[key] = np.recfromtxt(
                        cachelist[ifo], names=cachenames, encoding=None
                    )
                except:
                    # Python2
                    self.SFTCache[key] = np.recfromtxt(cachelist[ifo], names=cachenames)

        self.check_viterbi_params()

    def __str__(self):
        msg = "Viterbi Parameters\n"
        msg += "-" * len(msg) + "\n"
        for key, val in self.items():
            msg += "{0:<15}: {1}\n".format(key, val)
        return msg

    def check_viterbi_params(self):
        """
        self checker. make sure only supported params are defined
        """
        # loop over keys
        for key, val in self.items():
            print(key)
            # pass over search type key
            # check injection parameters
            if key in ViterbiParams.valid_params:
                if val is not None:
                    # make sure they have the correct data type
                    self[key] = ViterbiParams.valid_params[key][0](val)
            else:
                msg = "{0} is not a valid parameter"
                raise ValueError(msg.format(key))
