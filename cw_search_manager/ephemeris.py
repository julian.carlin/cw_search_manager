"""
Handle pulsar ephemeris data
"""

from lalapps.pulsarpputils import psr_par
from .cw_params import Params, SearchParams, InjectionParams
from collections import OrderedDict
import os
import json
# import numpy as np

# TODO Add ability to read pitkin `pulsars.txt` json file.


class EphemerisList(OrderedDict):
    """dict of ephemerides.
    keys are names of pulsars, values
    are `PulsarParameters objects
    """
    def __init__(self):
        super(EphemerisList, self).__init__()

    @classmethod
    def from_pulsars_file(cls, myfile):
        basedir = os.path.dirname(myfile)
        with open(myfile, 'r') as ff:
            pulsars = json.load(ff)
            ephem_list = cls()
            ephem_list.file_list = []
            for pulsar in pulsars:
                fname = os.path.join(basedir,
                                     pulsars[pulsar]['file'])
                ephem_list.file_list.append(fname)
                try:
                    ephem_list[pulsar] = PulsarParameters(fname)
                except KeyError:
                    print(pulsar)
                    raise ValueError('')
            return ephem_list


class PulsarParameters(psr_par):
    """renaming of psr_par class to follow class naming conventions
    (because I'm annoying)."""

    def __init__(self, filename):
        super(PulsarParameters, self).__init__(filename)

    def construct_injection(self, harmonic=2, **injection_kwargs):
        params = Params()
        params.injection = InjectionParams(**injection_kwargs)
        # binary parameters
        params.injection.orbitPeriod = self["PB"]
        params.injection.orbitasini = self["A1"]
        params.injection.orbitTp = self["T0"]
        params.injection.orbitEcc = self["ECC"]
        params.injection.orbitArgp = self["OM"]
        # others
        params.injection.Alpha = self["RA_RAD"]
        params.injection.Delta = self["DEC_RAD"]
        params.injection.Freq = harmonic * self["F0"]
        params.injection.f1dot = harmonic * self["F1"]
        if self["F2"] is not None:
            params.injection.f2dot = self["F2"]
        if self["F3"] is not None:
            params.injection.f3dot = self["F3"]
        params.injection.refTime = self["PEPOCH"]
        return params

    def construct_fstat_search(self, delta_factor=1e-2, harmonic=2, **fstat_kwargs):
        """
        construct fstat search from ephemeris data for narrowband
        known pulsar search.

        Parameters:
        -----------
        delta_factor : `float`
            Corresponds to delta in 2008 Crab Pulsar paper. Frequency band  =
            delta * gw frequency. This will get over written if a different
            frequency band is set in `fstat_kwargs`
        harmonic : float
            harmonic of rotation frequency for search.
        fstat_kwargs : `dict`
            keyword arguments that get supplied to SearchParams class for
            f-statistic.
        """
        # initialize
        params = Params()
        params.search = SearchParams(**fstat_kwargs)

        # overwrite RA and DEC with ephemeris
        params.search.Alpha = self["RA_RAD"]
        params.search.Delta = self["DEC_RAD"]

        # overwrite these if necessary
        # if "FreqBand" not in params.search or params.search.FreqBand is None:
        params.search.FreqBand = harmonic * self["F0"] * delta_factor
        # if "f1dotBand" not in params.search or params.search.f1dotBand is None:
        params.search.f1dotBand = harmonic * self["F1"] * delta_factor

        # center rotation frequency in the band
        # binary parameters
        params.search.Freq = harmonic * self["F0"] - params.search.FreqBand / 2.0
        params.search.orbitasini = self["A1"]
        params.search.orbitTp = self["T0"]
        params.search.orbitEcc = self["ECC"]
        params.search.orbitArgp = self["OM"]
        params.search.orbitPeriod = self["PB"]

        # center spin down in band
        params.search.f1dot = harmonic * self["F1"] - params.search.f1dotBand / 2.0
        if self["F2"] is not None:
            params.search.f2dot = self["F2"]
        if self["F3"] is not None:
            params.search.f3dot = self["F3"]
        params.search.refTime = self["PEPOCH"]
        return params
