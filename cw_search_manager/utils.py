import lal

def gw_waveform(h0, cosi, freq, f1dot):
    if f1dot is None:
        f1dot = 0
    def wf(dt):
        dphi = lal.TWOPI * (freq * dt + f1dot * 0.5 * dt**2)
        ap = h0 * (1.0 + cosi**2) / 2.0
        ax = h0 * cosi
        return dphi, ap, ax
    return wf

